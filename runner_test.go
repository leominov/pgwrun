package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestRunner_Run(t *testing.T) {
	var requestMap = make(map[string]int)
	log := logrus.WithField("env", "test")
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqID := r.Method + ":" + r.URL.Path
		if _, ok := requestMap[reqID]; !ok {
			requestMap[reqID] = 0
		}
		requestMap[reqID]++
		w.WriteHeader(http.StatusOK)
	}))
	defer ts.Close()

	ts500 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer ts500.Close()

	opt := &Options{
		PushURL: ts.URL,
		PushJob: "test",
	}
	runner := NewRunner(log, opt)
	err := runner.Run(nil)
	assert.NoError(t, err)

	opt.PushGroupLabels = map[string]string{
		"case": "2",
	}
	runner = NewRunner(log, opt)
	err = runner.Run([]string{"foobar"})
	assert.Error(t, err)
	assert.Equal(t, 1, requestMap["POST:/metrics/job/test/case/2"])

	opt.PushUsername = "foobar"
	opt.PushPassword = "password"
	opt.PushGroupLabels = map[string]string{
		"case": "3",
	}
	runner = NewRunner(log, opt)
	err = runner.Run([]string{"sleep", "0.1"})
	assert.NoError(t, err)
	assert.Equal(t, 1, requestMap["POST:/metrics/job/test/case/3"])

	opt.PushURL = ts500.URL
	opt.PushGroupLabels = map[string]string{
		"case": "4",
	}
	runner = NewRunner(log, opt)
	err = runner.Run([]string{"sleep", "0.1"})
	assert.NoError(t, err)
	_, ok := requestMap["POST:/metrics/job/test/case/4"]
	assert.False(t, ok)
}
