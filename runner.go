package main

import (
	"os/exec"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"github.com/sirupsen/logrus"
)

type Runner struct {
	pusher *push.Pusher
	log    *logrus.Entry
	o      *Options

	durationMetric                prometheus.Gauge
	startTimestampMetric          prometheus.Gauge
	completionTimestampMetric     prometheus.Gauge
	lastSuccessfulMetric          prometheus.Gauge
	lastSuccessfulTimestampMetric prometheus.Gauge
}

func NewRunner(log *logrus.Entry, o *Options) *Runner {
	pusher := push.New(o.PushURL, o.PushJob)
	if len(o.PushUsername) > 0 && len(o.PushPassword) > 0 {
		pusher.BasicAuth(o.PushUsername, o.PushPassword)
	}
	for name, val := range o.PushGroupLabels {
		pusher.Grouping(name, val)
	}
	r := &Runner{
		pusher: pusher,
		log:    log,
		o:      o,
		durationMetric: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: o.PushJob,
			Subsystem: "command",
			Name:      "duration",
			Help:      "Execution duration.",
		}),
		startTimestampMetric: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: o.PushJob,
			Subsystem: "command",
			Name:      "start_timestamp_seconds",
			Help:      "Start time.",
		}),
		completionTimestampMetric: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: o.PushJob,
			Subsystem: "command",
			Name:      "completion_timestamp_seconds",
			Help:      "Completion time.",
		}),
		lastSuccessfulMetric: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: o.PushJob,
			Subsystem: "command",
			Name:      "last_successful",
			Help:      "Completion status.",
		}),
		lastSuccessfulTimestampMetric: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: o.PushJob,
			Subsystem: "command",
			Name:      "last_successful_timestamp_seconds",
			Help:      "Last successful completion time.",
		}),
	}
	r.pusher.Collector(r.durationMetric)
	r.pusher.Collector(r.startTimestampMetric)
	r.pusher.Collector(r.completionTimestampMetric)
	r.pusher.Collector(r.lastSuccessfulMetric)
	return r
}

func (r *Runner) Run(command []string) error {
	var cmd *exec.Cmd

	if len(command) == 1 {
		cmd = exec.Command(command[0])
	} else if len(command) > 1 {
		cmd = exec.Command(command[0], command[1:]...)
	} else {
		r.log.Warn("Nothing to do")
		return nil
	}

	log := r.log.WithField("command", strings.Join(command, " "))
	cmd.Stdout = log.WriterLevel(logrus.InfoLevel)
	cmd.Stderr = log.WriterLevel(logrus.ErrorLevel)

	startTime := time.Now()
	r.startTimestampMetric.SetToCurrentTime()

	err := cmd.Run()

	r.durationMetric.Set(float64(time.Now().Sub(startTime).Milliseconds()))
	r.completionTimestampMetric.SetToCurrentTime()

	if err != nil {
		r.lastSuccessfulMetric.Set(0)
	} else {
		r.lastSuccessfulMetric.Set(1)

		// Do not add SuccessfulTimestampMetric on failure
		r.pusher.Collector(r.lastSuccessfulTimestampMetric)
		r.lastSuccessfulTimestampMetric.SetToCurrentTime()
	}

	errPush := r.pusher.Add()
	if errPush != nil {
		r.log.WithError(errPush).Error("Failed to push metrics")
	} else {
		r.log.Debugf("Successfully Pushed to %s", r.o.PushURL)
	}

	return err
}
