# pgwrun

Отправка метрик результата выполнения команды в Pushgateway.

## Настройки

Настраивается через переменные окружения:

* `PUSHGATEWAY_URL` (обязательно, по-умолчанию используется `https://pushgateway.infra.cloud.qlean.ru`)
* `PUSHGATEWAY_JOB` (обязательно)
* `PUSHGATEWAY_USERNAME`
* `PUSHGATEWAY_PASSWORD`
* `PUSHGATEWAY_LABELS` (например, `env:prod,instance:vm-postgres-db1`)
* `LOG_FORMAT` (`json`)
* `LOG_LEVEL` (`info`)

## Пример

```shell
export PUSHGATEWAY_URL=https://pushgateway.infra.cloud.qlean.ru
export PUSHGATEWAY_JOB=test_backup
export PUSHGATEWAY_LABELS=env:test,dbname:postgres,instance:localhost
pgwrun ./backup-database.sh
```

После того как скрипт завершит работу, метрики будут отправлены в Pushgateway, после чего по ним можно сделать Dashboard в Grafana и сделать алерт.

## Возможные проблемы

### Error while reading from Writer: bufio.Scanner: token too long

Например, можно встретить при выполнении команды `aws s3 cp`, когда большой файл копируется в удаленное хранилище и это сопровождается выводом прогресса. Решить проблему можно добавив флаг `--no-progress`.

## Ссылки

* https://console.cloud.google.com/storage/browser/devops-internal/pgwrun?project=qlean-242314
