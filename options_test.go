package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadOptionsFromEnv(t *testing.T) {
	_ = os.Unsetenv("PUSHGATEWAY_URL")
	_ = os.Unsetenv("PUSHGATEWAY_JOB")
	_, err := LoadOptionsFromEnv()
	assert.Error(t, err)

	_ = os.Setenv("PUSHGATEWAY_URL", "https://google.com")
	_ = os.Setenv("PUSHGATEWAY_JOB", "@#$%^")
	_, err = LoadOptionsFromEnv()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "not a valid name")
	}

	_ = os.Setenv("PUSHGATEWAY_URL", "https://google.com")
	_ = os.Setenv("PUSHGATEWAY_JOB", "test_job")
	_ = os.Setenv("PUSHGATEWAY_LABELS", "@#$%^:bar")
	_, err = LoadOptionsFromEnv()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "not a valid name")
	}

	_ = os.Setenv("PUSHGATEWAY_URL", "https://google.com")
	_ = os.Setenv("PUSHGATEWAY_JOB", "test_job")
	_ = os.Setenv("PUSHGATEWAY_LABELS", "foo:bar,bar:foo,instance:localhost")
	c, err := LoadOptionsFromEnv()
	if assert.NoError(t, err) {
		assert.Equal(t, "https://google.com", c.PushURL)
		assert.Equal(t, "test_job", c.PushJob)
		assert.Equal(t, "bar", c.PushGroupLabels["foo"])
		assert.Equal(t, "foo", c.PushGroupLabels["bar"])
		assert.Equal(t, "localhost", c.PushGroupLabels["instance"])
	}

	_ = os.Unsetenv("PUSHGATEWAY_LABELS")
	c, err = LoadOptionsFromEnv()
	if assert.NoError(t, err) {
		assert.NotEqual(t, "localhost", c.PushGroupLabels["instance"])
	}
}

func TestPrintsOptionsUsage(t *testing.T) {
	PrintsOptionsUsage()
}
