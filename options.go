package main

import (
	"fmt"
	"os"

	"github.com/kelseyhightower/envconfig"
	"github.com/prometheus/common/model"
)

type Options struct {
	PushURL         string            `envconfig:"PUSHGATEWAY_URL" default:"https://pushgateway.infra.cloud.qlean.ru" required:"true"`
	PushJob         string            `envconfig:"PUSHGATEWAY_JOB" required:"true"`
	PushUsername    string            `envconfig:"PUSHGATEWAY_USERNAME"`
	PushPassword    string            `envconfig:"PUSHGATEWAY_PASSWORD"`
	PushGroupLabels map[string]string `envconfig:"PUSHGATEWAY_LABELS"`
	LogFormat       string            `envconfig:"LOG_FORMAT" default:"json"`
	LogLevel        string            `envconfig:"LOG_LEVEL" default:"info"`
}

func LoadOptionsFromEnv() (*Options, error) {
	o := &Options{}
	err := envconfig.Process("", o)
	if err != nil {
		return nil, err
	}
	if !model.LabelName(o.PushJob).IsValid() {
		return nil, fmt.Errorf("%q is not a valid name for PUSHGATEWAY_JOB", o.PushJob)
	}
	if o.PushGroupLabels == nil {
		o.PushGroupLabels = make(map[string]string)
	}
	for name := range o.PushGroupLabels {
		if !model.LabelName(name).IsValid() {
			return nil, fmt.Errorf("%q is not a valid name for PUSHGATEWAY_LABELS", o.PushJob)
		}
	}
	if _, ok := o.PushGroupLabels["instance"]; !ok {
		o.PushGroupLabels["instance"], _ = os.Hostname()
	}
	return o, nil
}

func PrintsOptionsUsage() {
	o := &Options{}
	_ = envconfig.Usage("", o)
}
