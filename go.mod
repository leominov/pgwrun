module gitlab.qleanlabs.ru/platform/infra/pgwrun

go 1.16

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.26.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.4.0
)
