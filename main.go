package main

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

func main() {
	var (
		logger    *logrus.Entry
		runner    *Runner
		logFields = logrus.Fields{
			"app": "pgwrun",
		}
	)

	l := logrus.New()
	l.SetFormatter(&logrus.JSONFormatter{})

	o, err := LoadOptionsFromEnv()
	if err != nil {
		fmt.Println(err)
		fmt.Println()
		PrintsOptionsUsage()
		os.Exit(1)
	}

	if o.LogFormat == "text" {
		l.SetFormatter(&logrus.TextFormatter{})
	}

	if level, err := logrus.ParseLevel(o.LogLevel); err == nil {
		l.SetLevel(level)
	}

	for name, val := range o.PushGroupLabels {
		logFields[name] = val
	}
	logger = l.WithFields(logFields)

	runner = NewRunner(logger, o)
	err = runner.Run(os.Args[1:])
	if err != nil {
		logger.WithError(err).Error("Execution failed")
		os.Exit(1)
	}

	logger.Info("Done")
}
